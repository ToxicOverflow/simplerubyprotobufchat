 require_relative 'ruby/ChatProto_pb'
 require 'socket'
 require 'base64'

 class SocketHolder
   def initialize(s, t)
     @socket = s
     @token = t
   end

   def get_token
      @token
   end

   def get_socket
     @socket
   end
 end

 users = Hash.new
 socks = Array.new

 def generate_token(size)
   charset = %w{ 2 3 4 6 7 9 A C D E F G H J K M N P Q R T V W X Y Z}
   (0...size).map{ charset.to_a[rand(charset.size)] }.join
 end

def send(s, message)
  s.puts Base64.encode64 message
end

 Socket.tcp_server_loop(16807) {| sock, client_addrinfo |

   Thread.new {

      while line = Base64.decode64(sock.gets)
        puts line
        message = ChatProto::Message.decode(line)
        puts 'Got Message: ' + ChatProto::Message.encode_json( message )
        puts message.type

        if message.type == :GET_TOKEN_REQUEST # GET_TOKEN_REQUEST
          request = ChatProto::GetTokenRequest.decode(message.message)
          token = generate_token(10)
          users[token] = request.username
          socks.push(SocketHolder.new(sock, token))
          response = ChatProto::GetTokenResponse.new(:token => token)
          send(sock, ChatProto::Message.encode(ChatProto::Message.new(:type => :GET_TOKEN_RESPONSE, :message => ChatProto::GetTokenResponse.encode(response))))
        end

        if message.type == :SEND_MESSAGE_REQUEST # SEND_MESSAGE_REQUEST
          puts 'Send_Message'
          request = ChatProto::SendMessageRequest.decode(message.message)
          response = ChatProto::ReceiveMessageRequest.new(:username => users[message.token], :message => request.message)
          msg = ChatProto::Message.encode(ChatProto::Message.new(:type => :RECEIVE_MESSAGE_REQUEST, :message => ChatProto::ReceiveMessageRequest.encode(response)))
          socks.each do | socket|
            if  socket.get_token == message.token or socket.get_socket.closed? # make sure its not the sender and the socket is open
            else
              send(socket.get_socket, msg)
            end
          end
        end
      end
   }
 }