require_relative 'ruby/ChatProto_pb'
require 'socket'
require 'base64'
s = TCPSocket.new 'localhost', 16807

print 'Username: '
username = gets.chomp
token = ''

def send(s, message)
  s.puts Base64.encode64 message
end

t = Thread.new {
    while line = Base64.decode64(s.gets)
      message = ChatProto::Message.decode(line)

      if message.type == :GET_TOKEN_RESPONSE
        response = ChatProto::GetTokenResponse.decode(message.message)
        token = response.token
      end

      if message.type == :RECEIVE_MESSAGE_REQUEST
        response = ChatProto::ReceiveMessageRequest.decode(message.message)
        puts response.username + ': ' + response.message
      end
    end
}

send(s, ChatProto::Message.encode(ChatProto::Message.new(:type => :GET_TOKEN_REQUEST, :message => ChatProto::GetTokenRequest.encode(ChatProto::GetTokenRequest.new(:username => username)))))

puts 'Yo'

while token == ''
end

puts 'Got token: ' + token

t2 = Thread.new {
  while true
    print 'Enter a Message: '
    text = gets.chomp
    msg = ChatProto::SendMessageRequest.encode(ChatProto::SendMessageRequest.new(:message => text))
    send(s, ChatProto::Message.encode(ChatProto::Message.new(:type => :SEND_MESSAGE_REQUEST, :token => token,  :message => msg)))
  end
}

t.join
t2.join